

fizzbuzz="${FIZZBUZZ_BINARY:-fizzbuzz}"


function run_failed() {

  echo "$(date),run,FAILURE"
  run_failed="true"
}

trap run_failed ERR

features=""

result=$("${fizzbuzz}" 44)
if [ "$result" == "44" ] && [ "${run_failed}" != "true" ]; then
  features="${features} number"
fi

result=$("${fizzbuzz}" 66)
if [ "$result" == "fizz" ] && [ "${run_failed}" != "true" ]; then
  features="${features} fizz"
fi
result=$("${fizzbuzz}" 515)
if [ "$result" == "buzz" ] && [ "${run_failed}" != "true" ]; then
  features="${features} buzz"
fi
result=$("${fizzbuzz}" 480)
if [ "$result" == "fizzbuzz" ] && [ "${run_failed}" != "true" ]; then
  features="${features} fizzbuzz"
fi

echo "${features}"
