# tbk-fizzbuzz

Train for trunk based development with fizzbuzz. 

This repository has starter kit for
* golang
* dotnet (c#)

It uses the [tbk](https://gitlab.com/ydisanto/tbk) cli tool.

## Set up the kata

Install the [tbk](https://gitlab.com/ydisanto/tbk) cli tool.

Remove the `.git` folder of this project.

In you're tech folder (golang, dotnet, ...) run the `tbk setup` command.

Start the kata with a first commit (any message you want).
```shell
git commit -a -m "Initial commit"
```

Implement fizzbuzz and commit each time you want to "deploy" a new feature.

In the end compute your score.
```shell
tbk score
``` 
